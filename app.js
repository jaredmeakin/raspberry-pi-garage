const express = require('express')
const app = express()

// Middleware
const helmet = require('helmet')
const path = require('path')
const bodyParser = require('body-parser')
const logger = require('morgan')

// Configuration
app.use(helmet())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(logger('dev'))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// Routes
app.use('/', require('./routes/index'))
app.use('/status', require('./routes/status'))
app.use('/open', require('./routes/open'))
app.use('/close',require('./routes/close'))

app.use(express.static(path.join(__dirname, 'public')))

// Errors
app.use(function (err, req, res) {
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  res.status(err.status || 500)
  res.render('error')
})

app.listen('3001')
console.log('🚀  App running on port 3001!')
