const express = require('express')
const router = express.Router()
const GPIO = require('onoff').Gpio

router.get('/', (req, res) => {
  const garage = new GPIO(18, 'high') // 1 = High, 0 = Low
  const status = new GPIO(17, 'in') // 0 = Closed, 1 = Open

  if (status.readSync() === 0) {
    garage.writeSync(0)
    setTimeout(() => garage.writeSync(1), 1000)
    res.sendStatus(200)
  } else {
    res.sendStatus(304)
  }
})

module.exports = router
