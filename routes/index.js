const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  res.render('index', { server: process.env.SERVER })
})

module.exports = router
