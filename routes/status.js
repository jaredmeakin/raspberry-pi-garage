const express = require('express')
const router = express.Router()
const GPIO = require('onoff').Gpio

router.get('/', (req, res) => {
  const status = new GPIO(17, 'in') // 0 = Closed, 1 = Open
  const response = status.readSync() === 0 ? 'Closed' : 'Open'
  res.status(200).json({status: response})
})

module.exports = router
